<?php

use yii\db\Migration;

/**
 * Class m190928_054847_service
 */
class m190928_054847_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TYPE SERVICE_TYPE AS ENUM('appointment', 'check', 'payment')");

        $this->execute("
            CREATE TABLE public.service 
            (
                id BIGSERIAL NOT NULL,
                name CHARACTER VARYING NOT NULL,
                \"desc\" CHARACTER VARYING NOT NULL,
                type SERVICE_TYPE NOT NULL,
                CONSTRAINT service_pkey PRIMARY KEY (id)
            ) 
            WITH (
            OIDS = FALSE
            );
        ");

        $this->execute("
            CREATE TABLE public.service_step
            (
                id BIGSERIAL NOT NULL,
                service_id BIGINT NOT NULL,
                name CHARACTER VARYING NOT NULL,
                \"desc\" CHARACTER VARYING NOT NULL,
                step INT NOT NULL,
                parent_id BIGINT DEFAULT NULL,
                CONSTRAINT service_step_pkey PRIMARY KEY (id),
                CONSTRAINT uk_parent_id UNIQUE (parent_id),
                CONSTRAINT fk_service_step_service_id FOREIGN KEY (service_id)
                REFERENCES public.service (id) MATCH SIMPLE
                ON UPDATE RESTRICT
                ON DELETE RESTRICT
            )
            WITH (
            OIDS = FALSE
            );
        ");

        $this->execute("
            CREATE TABLE public.service_field
            (
                id BIGSERIAL NOT NULL,
                service_step_id BIGINT NOT NULL,
                name CHARACTER VARYING NOT NULL,
                \"desc\" CHARACTER VARYING NOT NULL,
                hint CHARACTER VARYING NOT NULL,
                type CHARACTER VARYING(16) NOT NULL,
                provider CHARACTER VARYING(255) NOT NULL,
                func CHARACTER VARYING(255) NOT NULL,
                validator CHARACTER VARYING(255) NOT NULL,
                CONSTRAINT service_field_pkey PRIMARY KEY (id),
                CONSTRAINT fk_service_field_service_step_id FOREIGN KEY (service_step_id)
                REFERENCES public.service_step (id) MATCH SIMPLE
                ON UPDATE RESTRICT
                ON DELETE RESTRICT
            )
            WITH (
            OIDS = FALSE
            );
        ");

        $this->execute("
            CREATE TABLE public.service_field_data 
            (
                id BIGSERIAL NOT NULL,
                service_field_id BIGINT NOT NULL,
                value CHARACTER VARYING NOT NULL,
                \"desc\" CHARACTER VARYING NOT NULL,
                type SERVICE_TYPE NOT NULL,
                CONSTRAINT service_field_data_pkey PRIMARY KEY (id),
                CONSTRAINT fk_service_field_data_id FOREIGN KEY (service_field_id)
                REFERENCES public.service_field (id) MATCH SIMPLE
                ON UPDATE RESTRICT
                ON DELETE RESTRICT
            ) 
            WITH (
            OIDS = FALSE
            );
        ");

        $this->execute("
            CREATE TABLE public.service_process
            (
                id BIGSERIAL NOT NULL,
                service_id BIGINT NOT NULL,
                status CHARACTER VARYING NOT NULL,
                stage CHARACTER VARYING(16) NOT NULL,
                sort_order INT NOT NULL DEFAULT 0,
                CONSTRAINT service_process_pkey PRIMARY KEY (id),
                CONSTRAINT fk_service_process_service_id FOREIGN KEY (service_id)
                REFERENCES public.service (id) MATCH SIMPLE
                ON UPDATE RESTRICT
                ON DELETE RESTRICT
            )
            WITH (
            OIDS = FALSE
            );
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->execute("DROP TYPE IF EXISTS SERVICE_TYPE CASCADE");
       $this->execute("DROP TABLE public.service_process CASCADE");
       $this->execute("DROP TABLE public.service_field_data CASCADE");
       $this->execute("DROP TABLE public.service_field CASCADE");
       $this->execute("DROP TABLE public.service_step CASCADE");
       $this->execute("DROP TABLE public.service CASCADE");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190928_054847_service cannot be reverted.\n";

        return false;
    }
    */
}
