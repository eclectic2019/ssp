<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%service_process}}".
 *
 * @property int $id
 * @property int $service_id
 * @property string $status
 * @property string $stage
 * @property int $sort_order
 *
 * @property Service $service
 */
class ServiceProcess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%service_process}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'status', 'stage'], 'required'],
            [['service_id', 'sort_order'], 'default', 'value' => null],
            [['service_id', 'sort_order'], 'integer'],
            [['status'], 'string'],
            [['stage'], 'string', 'max' => 16],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_id' => Yii::t('app', 'Service ID'),
            'status' => Yii::t('app', 'Status'),
            'stage' => Yii::t('app', 'Stage'),
            'sort_order' => Yii::t('app', 'Sort Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}
