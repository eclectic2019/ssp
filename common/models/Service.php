<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%service}}".
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property string $type
 *
 * @property ServiceProcess[] $serviceProcesses
 * @property ServiceStep[] $serviceSteps
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'type'], 'required'],
            [['name', 'desc', 'type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Service Name'),
            'desc' => Yii::t('app', 'Service Desc'),
            'type' => Yii::t('app', 'Service Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceProcesses()
    {
        return $this->hasMany(ServiceProcess::className(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceSteps()
    {
        return $this->hasMany(ServiceStep::className(), ['service_id' => 'id']);
    }
}
