<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%service_step}}".
 *
 * @property int $id
 * @property int $service_id
 * @property string $name
 * @property string $desc
 * @property int $step
 * @property int $parent_id
 *
 * @property ServiceField[] $serviceFields
 * @property Service $service
 */
class ServiceStep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%service_step}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'name', 'desc', 'step'], 'required'],
            [['service_id', 'step', 'parent_id'], 'default', 'value' => null],
            [['service_id', 'step', 'parent_id'], 'integer'],
            [['name', 'desc'], 'string'],
            [['parent_id'], 'unique'],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_id' => Yii::t('app', 'Service'),
            'name' => Yii::t('app', 'Step Name'),
            'desc' => Yii::t('app', 'Step Desc'),
            'step' => Yii::t('app', 'Step Order'),
            'parent_id' => Yii::t('app', 'Parent ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceFields()
    {
        return $this->hasMany(ServiceField::className(), ['service_step_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @param array $fields
     * @param array $expand
     * @param bool $recursive
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $arr = [];
        if ($this->serviceFields) {
            foreach ($this->serviceFields as $serviceField) {
                $arr[] = $serviceField;
            }
        }

        return ArrayHelper::merge(parent::toArray($fields, $expand, $recursive), [
            'fields' => $arr,
        ]);
    }
}
