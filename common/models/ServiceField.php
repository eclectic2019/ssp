<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%service_field}}".
 *
 * @property int $id
 * @property int $service_step_id
 * @property string $name
 * @property string $desc
 * @property string $hint
 * @property string $type
 * @property string $provider
 * @property string $func
 * @property string $validator
 *
 * @property ServiceStep $serviceStep
 * @property ServiceFieldData[] $serviceFieldData
 */
class ServiceField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%service_field}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_step_id', 'name', 'desc', 'hint', 'type', 'provider', 'func', 'validator'], 'required'],
            [['service_step_id'], 'default', 'value' => null],
            [['service_step_id'], 'integer'],
            [['name', 'desc', 'hint'], 'string'],
            [['type'], 'string', 'max' => 16],
            [['provider', 'func', 'validator'], 'string', 'max' => 255],
            [['service_step_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceStep::className(), 'targetAttribute' => ['service_step_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_step_id' => Yii::t('app', 'Service Step ID'),
            'name' => Yii::t('app', 'Name'),
            'desc' => Yii::t('app', 'Desc'),
            'hint' => Yii::t('app', 'Hint'),
            'type' => Yii::t('app', 'Type'),
            'provider' => Yii::t('app', 'Provider'),
            'func' => Yii::t('app', 'Func'),
            'validator' => Yii::t('app', 'Validator'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceStep()
    {
        return $this->hasOne(ServiceStep::className(), ['id' => 'service_step_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceFieldData()
    {
        return $this->hasMany(ServiceFieldData::className(), ['service_field_id' => 'id']);
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        $provider = [];
        if ($this->provider !== '') {
            $provider = explode(',', $this->provider);
        }
        $validator = [];
        if ($this->validator) {
            $validator = explode(',', $this->validator);
        }

        return ArrayHelper::merge(parent::toArray($fields, $expand, $recursive), [
            'provider' => $provider,
            'validator' => $validator,
        ]);
    }
}
