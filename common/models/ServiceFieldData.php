<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%service_field_data}}".
 *
 * @property int $id
 * @property int $service_field_id
 * @property string $value
 * @property string $desc
 * @property string $type
 *
 * @property ServiceField $serviceField
 */
class ServiceFieldData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%service_field_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_field_id', 'value', 'desc', 'type'], 'required'],
            [['service_field_id'], 'default', 'value' => null],
            [['service_field_id'], 'integer'],
            [['value', 'desc', 'type'], 'string'],
            [['service_field_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceField::className(), 'targetAttribute' => ['service_field_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_field_id' => Yii::t('app', 'Service Field ID'),
            'value' => Yii::t('app', 'Value'),
            'desc' => Yii::t('app', 'Desc'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceField()
    {
        return $this->hasOne(ServiceField::className(), ['id' => 'service_field_id']);
    }
}
