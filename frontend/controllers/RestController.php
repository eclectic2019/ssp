<?php

namespace frontend\controllers;

use common\models\Service;
use common\models\ServiceField;
use common\models\ServiceProcess;
use common\models\ServiceStep;
use yii\db\Exception;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

class RestController extends Controller
{
    public function actionAddService()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $body = json_decode(Yii::$app->request->getRawBody(), true);

        $srv = $body['service'];

        $tr = Yii::$app->getDb()->beginTransaction();
        try {
            $service = new Service();
            $service->name = $srv['name'];
            $service->desc = $srv['desc'];
            $service->type = $srv['type'];

            if ($service->save(false)) {
                $steps = $body['steps'];
                foreach ($steps as $index => $step) {
                    $serviceStep = new ServiceStep();
                    $serviceStep->service_id = $service->id;
                    $serviceStep->name = $step['name'];
                    $serviceStep->desc = $step['desc'];
                    $serviceStep->step = $index;

                    if (!$serviceStep->save(false)) {
                        throw new Exception(json_encode($serviceStep->getErrors()));
                    }


                    if (!empty($step['fields'])) {
                        $fields = $step['fields'];

                        foreach ($fields as $idx => $field) {
                            $serviceField = new ServiceField();
                            $serviceField->name = $field['name'];
                            $serviceField->desc = $field['desc'];
                            $serviceField->hint = $field['hint'];
                            $serviceField->type = $field['type'];
                            $serviceField->provider = '';
                            $serviceField->validator = '';
                            $serviceField->func = '';
                            if (count($field['provider']) > 0) {
                                $serviceField->provider = join(',', $field['provider']);
                            }

                            if (count($field['validator']) > 0) {
                                $serviceField->validator = join(',', $field['validator']);
                            }

                            $serviceField->service_step_id = $serviceStep->id;

                            if (!$serviceField->save(false)) {
                                throw new Exception(json_encode($serviceField->getErrors()));
                            }
                        }
                    }
                }

                $processes = $body['processes'];
                foreach ($processes as $sortOrder => $process) {
                    $serviceProcess = new ServiceProcess();
                    $serviceProcess->service_id = $service->id;
                    $serviceProcess->sort_order = $sortOrder;
                    $serviceProcess->status = $process['status'];
                    $serviceProcess->stage = $process['stage'];

                    if (!$serviceProcess->save(false)) {
                        throw new Exception(json_encode($serviceProcess->getErrors()));
                    }
                }
            } else {
                throw new Exception(json_encode($service->getErrors()));
            }
            $tr->commit();
        } catch (\Exception $exception) {
            $tr->rollBack();
            throw $exception;
        }

        return [
            'success' => true,
        ];
    }
}
