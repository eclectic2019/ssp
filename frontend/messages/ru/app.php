<?php

return [
    'Services' => 'Услуги',
    'Create Service' => 'Создание услуги',
    'Update Service' => 'Редактирование услуги',
    'Update' => 'Редактирование',
    'Edit' => 'Изменить',
    'Delete' => 'Удалить',
    'Login' => 'Войти',
    'Appointment' => 'Запись',
    'Payment' => 'Оплата',
    'Check' => 'Проверка',
    'Service Type' => 'Тип услуги',
    'Service Desc' => 'Описание услуги',
    'Service Name' => 'Название услуги',
    'Next' => 'Делее',
    'Step Name' => 'Название группы',
    'Step Desc' => 'Описание группы',
    '' => '',
];
