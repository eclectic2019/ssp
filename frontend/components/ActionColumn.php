<?php

namespace frontend\components;

use Yii;
use yii\bootstrap\Html;

class ActionColumn extends \yii\grid\ActionColumn {
    /**
     * @var string
     */
    public $template = '<div class="btn-group">{update} {delete}</div>';

    public $contentOptions = ['class' => 'text-nowrap'];
    public $headerOptions = ['style' => 'width: 200px'];

    /**
     * @inheritdoc
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = false;
        }

        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Edit'),
                    'aria-label' => Yii::t('app', 'Edit'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-primary btn-sm'
                ], $this->buttonOptions);

                return Html::a('<i class="glyphicon glyphicon-edit"></i> ' . Yii::t('app', 'Edit'), $url, $options);
            };
        }

        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Delete'),
                    'aria-label' => Yii::t('app', 'Delete'),
                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-danger btn-sm'
                ], $this->buttonOptions);
                return Html::a('<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Delete'), $url, $options);
            };
        }

        return parent::initDefaultButtons();
    }
}