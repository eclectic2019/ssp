<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Services');
$this->params['breadcrumbs'][] = $this->title;

$types = [
    'appointment' => Yii::t('app', 'Appointment'),
    'check' => Yii::t('app', 'Check'),
    'payment' =>  Yii::t('app', 'Payment'),
];
?>
<div class="service-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Service'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'id' => [
                'attribute' => 'id',
                'headerOptions' => [
                    'style' => 'width: 70px'
                ]
            ],
            'name' => [
                'attribute' => 'name',
                'format' => 'raw',
                'contentOptions' => [
                    'style' => 'white-space: inherit',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Service */
                    $res = Html::tag('p', Html::a($model->name, Url::to(['service/view', 'id' => $model->id])));
                    if ($model->desc !== "") {
                        $res .= Html::tag('p', $model->desc);
                    }
                    return $res;
                }
            ],
            'type' => [
                'attribute' => 'type',
                'format' => 'raw',
                'headerOptions' => [
                    'style' => 'width: 200px'
                ],
                'value' => function ($model) use ($types) {
                    /* @var $model \common\models\Service */
                    return $types[$model->type];
                },
            ],

            ['class' => 'frontend\components\ActionColumn'],
        ],
    ]); ?>


</div>
