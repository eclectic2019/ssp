<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $form yii\widgets\ActiveForm */
/* @var $step \common\models\ServiceStep */
/* @var $field \common\models\ServicField */

$types = [
    'appointment' => Yii::t('app', 'Appointment'),
    'check' => Yii::t('app', 'Check'),
    'payment' =>  Yii::t('app', 'Payment'),
];
?>

<div class="service-form panel-group" role="tablist" aria-multiselectable="true">

<!--    --><?php //$form = ActiveForm::begin(); ?>
    <div class="panel panel-default" id="service-panel">
        <div class="panel-heading" role="tab" id="heading-1">
            <h5><a role="button" data-toggle="collapse" data-parent="service-panel" href="#collapse-1" aria-expanded="true" aria-controls="collapse-1">Шаг 1: Описание услуги</a></h5>
<!--            <h5 class="shim-heading">Шаг 1: Описание услуги</h5>-->
        </div>
        <div class="panel-body panel-collapse collapse in" id="collapse-1" role="tabpanel" aria-labelledby="heading-2">
            <form action="#" id="service-form">
                <div class="row">
                    <div class="col-sm-9">
                         <div class="form-group">
                             <label class="control-label" for="<?=Html::getInputId($model, 'name')?>">
                                 <?=Yii::t('app', 'Service Name')?>
                             </label>
                             <input type="text" class="form-control"
                                    name="<?=Html::getInputName($model, 'name')?>"
                                    value="<?=$model->name?>"
                                    id="<?=Html::getInputId($model, 'name')?>"
                             />
                         </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="<?=Html::getInputId($model, 'type')?>">
                                <?=Yii::t('app', 'Service Type')?>
                            </label>
                            <select
                                    class="form-control"
                                    name="<?=Html::getInputName($model, 'type')?>"
                                    id="<?=Html::getInputId($model, 'type')?>"
                            >
                                <?php foreach ($types as $key => $type): ?>
                                <option value="<?=$key?>"><?=$type?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="<?=Html::getInputId($model, 'desc')?>">
                        <?=Yii::t('app', 'Service Desc')?>
                    </label>
                    <textarea
                            rows="4"
                            class="form-control"
                            id="<?=Html::getInputId($model, 'desc')?>"
                            name="<?=Html::getInputName($model, 'desc')?>"
                    ><?=$model->desc?></textarea>
                </div>
                <p class="text-right">
                    <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'Next') ?></button>
                </p>
            </form>
        </div>
    </div>
    <div class="panel panel-default" id="service-steps">
        <div class="panel-heading" role="tab" id="heading-2">
            <h5>
                <a role="button" data-toggle="collapse" data-parent="service-steps" href="#collapse-2" aria-expanded="false" aria-controls="collapse-2">Шаг 2: Форма</a>
            </h5>
        </div>
        <div class="panel-body panel-collapse collapse" id="collapse-2" role="tabpanel" aria-labelledby="heading-2">
            <form action="#">
                <p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#step-modal"><i class="glyphicon glyphicon-plus"></i> Добавить группу полей</button>
                </p>
            </form>
            <table class="table table-condensed table-hover" id="step-list">
                <tbody></tbody>
            </table>
            <p>&nbsp;</p>
            <p class="text-right">
                <button type="button" class="btn btn-success" id="next-3"><?= Yii::t('app', 'Next') ?></button>
            </p>
        </div>
    </div>
    <div class="panel panel-default" id="service-process">
        <div class="panel-heading" role="tab" id="heading-3">
            <h5>
                <a role="button" data-toggle="collapse" data-parent="service-steps" href="#collapse-3" aria-expanded="false" aria-controls="collapse-3">Шаг 3: Процесс</a>
            </h5>
        </div>
        <div class="panel-body panel-collapse collapse" id="collapse-3" role="tabpanel" aria-labelledby="heading-3">
            <table class="table table-striped table-condensed" id="statuses">
                <thead>
                <tr>
                    <th>Этап процесса</th>
                    <th>Статус</th>
                    <th style="width: 90px">Действие</th>
                </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                <tr>
                    <td><input type="text" class="form-control input-sm" id="process-status" /></td>
                    <td>
                        <select class="form-control input-sm" id="process-stage">
                            <option value="new">Заявление поступило</option>
                            <option value="incomplete">Неполные данные</option>
                            <option value="accepted">Принято к рассмотрению</option>
                            <option value="reviewed">Рассмотрено</option>
                        </select>
                    </td>
                    <td class="text-right">
                        <button class="btn btn-primary btn-sm" onclick="addProcess()">
                            <i class="glyphicon glyphicon-plus"></i>
                            Добавить
                        </button>
                    </td>
                </tr>
                </tfoot>
            </table>
            <p>&nbsp;</p>

            <p class="text-right">
                <button type="button" class="btn btn-success" id="save">Сохранить</button>
            </p>
        </div>
    </div>

<!--    --><?php //ActiveForm::end(); ?>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="step-modal">
    <div class="modal-dialog" role="document">
        <form action="#" id="step-form" data-step="-1">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Создание группы полей</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label"><?=Yii::t('app', 'Step Name')?></label>
                                <input type="text" class="form-control"
                                       name="<?=Html::getInputName($step, 'name')?>"
                                       value=""
                                       id="<?=Html::getInputId($step, 'name')?>"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label" for="<?=Html::getInputId($step, 'desc')?>">
                                    <?=Yii::t('app', 'Step Desc')?>
                                </label>
                                <textarea
                                        rows="4"
                                        class="form-control"
                                        id="<?=Html::getInputId($step, 'desc')?>"
                                        name="<?=Html::getInputName($step, 'desc')?>"
                                ><?=$step->desc?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button class="btn btn-success" type="submit">Сохранить</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade in" tabindex="-1" role="dialog" id="step-fields-modal">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Поля <span class="step-name"></span></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-condensed table-striped" id="field-list">
                        <thead>
                        <tr>
<!--                            <th>#</th>-->
                            <th>Название</th>
                            <th>Тип</th>
                            <th>Провайдер</th>
                            <th>Валидатор</th>
                            <th style="width: 100px">
                                <button type="button" class="btn btn-link" onclick="stepField()">
                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                    Добавить
                                </button>
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                </div>
            </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/html" id="template-step-list">
    <tr id="step-{{step}}">
        <td>{{name}}</td>
        <td style="width: 150px" class="text-right">
            <div class="btn-group">
                <button class="btn btn-link btn-sm" type="button" onclick="stepFields({{step}})">
                    <i class="glyphicon glyphicon-list-alt"></i> Поля
                </button>
                <button class="btn btn-primary btn-sm" type="button" onclick="editStep({{step}})">
                    <i class="glyphicon glyphicon-edit"></i>
                </button>
                <button class="btn btn-danger btn-sm" type="button" onclick="removeStep({{step}})">
                    <i class="glyphicon glyphicon-trash"></i>
                </button>
            </div>
        </td>
    </tr>
</script>
<script type="text/html" id="template-field-list">
    <tr id="field-{{step}}-{{row}}">
<!--        <td>{{row}}</td>-->
        <td>{{name}}<br>{{desc}}</td>
        <td>{{type}}</td>
        <td>{{provider}}</td>
        <td>{{validator}}</td>
        <td class="text-right">
            <button type="button" class="btn btn-primary btn-sm" onclick="editField({{step}}, {{row}})">
                <i class="glyphicon glyphicon-edit"></i>
            </button>
            <button type="button" class="btn btn-danger btn-sm" onclick="removeField({{step}}, {{row}})">
                <i class="glyphicon glyphicon-remove-sign"></i>
            </button>
        </td>
    </tr>
</script>
<div class="modal fade in" tabindex="-1" role="dialog" id="field-modal">
    <div class="modal-dialog modal-lg" role="document">
        <form action="#" id="field-form" data-step="-1">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        <span id="field-create-text">Создание поля</span>
                        <span id="field-edit-text">Редактирование поля</span>
                        <span class="step-name"></span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="<?=Html::getInputId($field, 'name')?>">Название поля:</label>
                        <input type="text" class="form-control" id="<?=Html::getInputId($field, 'name')?>" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="<?=Html::getInputId($field, 'desc')?>">Описание поля (подсказка):</label>
                        <textarea class="form-control" id="<?=Html::getInputId($field, 'desc')?>" rows="3"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="<?=Html::getInputId($field, 'type')?>">Тип поля:</label>
                                <select id="<?=Html::getInputId($field, 'type')?>" class="form-control">
                                    <option value="input">Текст</option>
                                    <option value="number">Число</option>
                                    <option value="date">Дата</option>
                                    <option value="text">Многострочный текст</option>
                                    <option value="select">Выпадающий список</option>
                                    <option value="multiple">Выпадающий список (несколько значений)</option>
                                    <option value="checkbox">Галочка (флажок)</option>
                                    <option value="radio">Галочка (флажок) (несколько значений)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="<?=Html::getInputId($field, 'hint')?>">Значение по умолчанию:</label>
                                <input type="text" class="form-control" id="<?=Html::getInputId($field, 'hint')?>" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="<?=Html::getInputId($field, 'provider')?>">Источник данных:</label>
                                <select id="<?=Html::getInputId($field, 'provider')?>" class="form-control" multiple size="9">
                                    <option value="internal">SPP (заполняется администратором)</option>
                                    <option value="Esia.SNILS">ЕСИА (СНИЛС)</option>
                                    <option value="Esia.FIO">ЕСИА (ФИО)</option>
                                    <option value="Esia.Bdate">ЕСИА (Дата рождения)</option>
                                    <option value="Esia.INN">ЕСИА (ИНН)</option>
                                    <option value="Esia.Passport">ЕСИА (общегражданский паспорт)</option>
                                    <option value="Esia.InternationalPassport">ЕСИА (заграничный паспорт)</option>
                                    <option value="Esia.DriverLicense">ЕСИА (водительское удостоверение)</option>
                                    <option value="Esia.militaryID">ЕСИА (военный билет)</option>
                                    <option value="Esia.OMS">ЕСИА (полис ОМС)</option>
                                    <option value="Esia.Email">ЕСИА (email)</option>
                                    <option value="Esia.Phone">ЕСИА (телефон)</option>
                                    <option value="Esia.Address">ЕСИА (адрес)</option>
                                    <option value="IFNS.INN">ИФНС (данные организации по ИНН)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="<?=Html::getInputId($field, 'validator')?>">Валидатор данных:</label>
                                <select id="<?=Html::getInputId($field, 'validator')?>" class="form-control" multiple size="9">
                                    <option value="Internal.Number">SPP число</option>
                                    <option value="Internal.Required">SPP обязательное поле</option>
                                    <option value="IFNS.INN">ИФНС (ИНН)</option>
                                    <option value="IFNS.EGRIP">ИФНС (ЕГРИП)</option>
                                    <option value="GIS_GMP.IDAndDate">ГИС ГМП (номер и дата документа)</option>
                                    <option value="FFOMS.OMS">ФФОМС (полис ОМС)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/html" id="template-process">
<tr>
    <td>{{status}}</td>
    <td>{{stage}}</td>
    <td><button type="button" class="btn btn-link" onclick="removeProcess({{row}})"><i class="glyphicon glyphicon-remove"></i>Удалить</button></td>
</tr>
</script>
<script type="text/javascript">
    let serviceData = <?=\yii\helpers\Json::encode($model)?>;
    <?php if (count($model->serviceSteps) > 0): ?>
    let serviceSteps = <?=\yii\helpers\Json::encode($model->serviceSteps)?>;
    <?php else: ?>
    let serviceSteps = [];
    <?php endif; ?>
    $(document).ready(function () {
        renderSteps($);
        $('#service-form').on('submit', function (e) {
            e.preventDefault();
            const form = $(this);
            dropErrs(form);
            let name = form.find('#service-name');
            let desc = form.find('#service-desc');
            let type = form.find('#service-type');
            let hasErrors = false;
            if (name.val() === '') {
                hasErrors = true;
                addError(name, 'Заполните название услуги')
            }
            if (type.val() === '') {
                hasErrors = true;
                addError(type, 'Выберите тип услуги')
            }

            if (hasErrors) {
                return;
            }

            serviceData.desc = name.val();
            serviceData.name = desc.val();
            serviceData.type = type.val();

            // $('#service-panel .panel-heading h5 a').toggle('click');
            // $('#service-panel .panel-heading h5 a').show();
            // $('#service-panel .panel-body').removeClass('in');
            // $('#service-panel .panel-heading h5').removeClass('hidden');
            $('#collapse-1').collapse('hide');
            $('#collapse-2').collapse('show');
        });

        $('#step-form').on('submit', function (e) {
            e.preventDefault();

            let form = $(this);
            const step = parseInt(form.attr('data-step'));
            const id = form.attr('data-id');

            dropErrs(form);
            let name = $('#servicestep-name');
            let desc = $('#servicestep-desc');
            let hasErrors = false;
            if (name.val() === '') {
                hasErrors = true;
                addError(name, "Введите название группы");
            }

            if (hasErrors) {
                return;
            }

            if (step < 0) {
                let data = {
                    id: null,
                    name: name.val(),
                    desc: desc.val(),
                };
                serviceSteps.push(data)
            } else {
                serviceSteps[step].name = name.val();
                serviceSteps[step].desc = desc.val();
            }

            renderSteps($);
        });

        $('#step-modal').on('hide.bs.modal', function () {
            $('#step-form').attr('data-step', -1);
            $('#servicestep-name').val('');
            $('#servicestep-desc').val('');
        });

        $('#field-form').on('submit', function (e) {
            e.preventDefault();

            let form = $(this);
            dropErrs(form);
            const step = parseInt(form.attr('data-step'));
            const field = parseInt(form.attr('data-field'));

            let name = $('#servicefield-name');
            let desc = $('#servicefield-desc');
            let type  = $('#servicefield-type');
            let hint  = $('#servicefield-hint');
            let provider  = $('#servicefield-provider');
            let validator  = $('#servicefield-validator');

            // console.log(serviceSteps, step);
            // if (!serviceSteps[step]) {
            //     return;
            // }

            if (!serviceSteps[step].hasOwnProperty('fields')) {
                serviceSteps[step].fields = [];
            }
            let fields = serviceSteps[step].fields.length;

            let hasErrors = false;
            if (name.val() === '') {
                hasErrors = true;
                addError(name, 'Введите название поля');
            }
            if (type.val() === '') {
                hasErrors = true;
                addError(name, 'Выберите тип поля');
            }

            if (hasErrors) {
                return;
            }

            let data = {
                name: name.val(),
                desc: desc.val(),
                hint: hint.val(),
                type: type.val(),
                provider: provider.val(),
                validator: validator.val(),
            };
            if (field >= 0) {
                serviceSteps[step].fields[field] = data;
            } else {
                if (fields === 0) {
                    serviceSteps[step].fields = [];
                }
                serviceSteps[step].fields.push(data);
            }

            renderFields($, step);
            $('#field-modal').modal('hide');
        });

        $('#field-modal').on('show.bs.modal', function () {
            let step = parseInt($('#field-form').attr('data-step'));
            if (step && step >= 0) {
                $('#field-create-text').hide();
                $('#field-edit-text').show();
            } else {
                $('#field-create-text').show();
                $('#field-edit-text').hide();
            }
        }).on('hide.bs.modal', function () {
            $('#field-form').attr('data-step', -1);
            $('#field-form').attr('data-field', -1);
            $('#servicefield-name').val('');
            $('#servicefield-desc').val('');
            $('#servicefield-hint').val('');
            $('#servicefield-type').find('option').prop('selected', false);
            $('#servicefield-type').trigger('click');
            $('#servicefield-provider').find('option').prop('selected', false);
            $('#servicefield-provider').trigger('click');
            $('#servicefield-validator').find('option').prop('selected', false);
            $('#servicefield-validator').trigger('click');
        });

        $('#next-3').on('click', function (e) {
            if (serviceSteps.length === 0) {
                alert('Создайте группы и поля формы!');
                return;
            }

            $('#collapse-1').collapse('hide');
            $('#collapse-2').collapse('hide');
            $('#collapse-3').collapse('show');
        });

        $('#save').on('click', function (e) {
            const req = {
                service: serviceData,
                steps: serviceSteps,
                processes: serviceStages,
            };

            $.ajax({
                url: '/service/add',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(req),
                success: function (data) {
                    if (data.hasOwnProperty('success') && data.success) {
                        window.location.href = '/service/index';
                    } else {
                        alert(data);
                    }
                }
            });
            console.log(JSON.stringify(req));
        });
    });



    function addError(field, err) {
        let errHtml = '<span class="help-block">' + err + '</span>';
        field.parent().addClass('errors');
        field.parent().append(errHtml);
    }

    function dropErrs(el) {
        el.find('.errors').removeClass('errors');
        el.find('.error').remove();
    }

    function renderSteps($) {
        if (serviceSteps.length === 0) {
            return
        }

        let html = '';
        const template = $('#template-step-list').html();

        for (let i = 0; i < serviceSteps.length; i++) {
            if (serviceSteps[i]) {
                let row = template.replace(/{{name}}/g, serviceSteps[i].name);
                row = row.replace(/{{id}}/g, serviceSteps[i].id);
                row = row.replace(/{{step}}/g, i);
                html += row;
            }
        }

        $('#step-list tbody tr').remove();
        $('#step-list tbody').append(html);
        $('#step-modal').modal('hide');
    }

    function removeStep(step) {
        delete serviceSteps[step];
        $('#step-' + step).remove();
    }

    function editStep(step) {
        $('#step-form').attr('data-step', step);
        $('#servicestep-name').val(serviceSteps[step].name);
        $('#servicestep-desc').val(serviceSteps[step].desc);
        $('#step-modal').modal('show');
    }

    function stepFields(step) {
        $('#field-form').attr('data-step', step);
        $('#step-fields-modal').attr('data-step', step);
        $('#step-fields-modal').modal('show');
        renderFields(jQuery, step);
    }

    function stepField(field) {
        $('#field-form').attr('data-step', $('#step-fields-modal').attr('data-step'));
        $('#field-form').attr('data-field', field);
        $('#field-modal').modal('show');
    }

    const fieldTypes = {
        'input': 'Текст',
        'number': 'Число',
        'date': 'Дата',
        'text': 'Многострочный текст',
        'select': 'Выпадающий список',
        'multiple': 'Выпадающий список (несколько значений)',
        'checkbox': 'Галочка (флажок)',
        'radio': 'Галочка (флажок) (несколько значений)',
    };
    const providers = {
        'internal': 'SPP (заполняется администратором)',
        'Esia.SNILS': 'ЕСИА (СНИЛС)',
        'Esia.FIO': 'ЕСИА (ФИО)',
        'Esia.Bdate': 'ЕСИА (Дата рождения)',
        'Esia.INN': 'ЕСИА (ИНН)',
        'Esia.Passport': 'ЕСИА (общегражданский паспорт)',
        'Esia.InternationalPassport': 'ЕСИА (заграничный паспорт)',
        'Esia.DriverLicense': 'ЕСИА (водительское удостоверение)',
        'Esia.militaryID': 'ЕСИА (военный билет)',
        'Esia.OMS': 'ЕСИА (полис ОМС)',
        'Esia.Email': 'ЕСИА (email)',
        'Esia.Phone': 'ЕСИА (телефон)',
        'Esia.Address': 'ЕСИА (адрес)',
        'IFNS.INN': 'ИФНС (данные организации по ИНН)',
    };
    const validators = {
        'Internal.Number': 'SPP число',
        'Internal.Required': 'SPP обязательное поле',
        'IFNS.INN': 'ИФНС (ИНН)',
        'IFNS.EGRIP': 'ИФНС (ЕГРИП)',
        'GIS_GMP.IDAndDate': 'ГИС ГМП (номер и дата документа)',
        'FFOMS.OMS': 'ФФОМС (полис ОМС)',
    };
    function renderFields($, step) {
        let html = '';

        let fields = serviceSteps[step].fields;
        console.log(fields, step)
        const tpl = $('#template-field-list').html();

        for (let i in fields) {
            if (fields[i]) {
                let row = tpl.replace(/{{name}}/g, fields[i].name);
                row = row.replace(/{{desc}}/g, fields[i].desc);
                row = row.replace(/{{hint}}/g, fields[i].hint);
                row = row.replace(/{{type}}/g, fieldTypes[fields[i].type]);
                let provs = '';
                for (let j in fields[i].provider) {
                    provs += providers[fields[i].provider[j]];
                    provs += '<br>';
                }
                row = row.replace(/{{provider}}/g, provs);
                let vals = '';
                for (let j in fields[i].validator) {
                    vals += validators[fields[i].validator[j]];
                    vals += '<br>';
                }
                row = row.replace(/{{validator}}/g, vals);
                row = row.replace(/{{row}}/g, i);
                row = row.replace(/{{step}}/g, step);
                html += row
            }
        }

        $('#field-list tbody').html(html);
    }

    function removeField(step, field) {
        delete serviceSteps[step].fields[field];
        renderFields(jQuery, step);
    }

    function editField(step, field) {
        let form = $('#field-form');
        dropErrs(form);

        let name = $('#servicefield-name');
        let desc = $('#servicefield-desc');
        let type  = $('#servicefield-type');
        let hint  = $('#servicefield-hint');
        let provider  = $('#servicefield-provider');
        let validator  = $('#servicefield-validator');

        let f = serviceSteps[step].fields[field];

        name.val(f.name);
        desc.val(f.desc);
        hint.val(f.hint);
        type.find('option[value="'+f.type+'"]').prop('selected', true);
        type.trigger('change');
        for (let i in f.provider) {
            provider.find('option[value="'+f.provider[i]+'"]').prop('selected', true);
        }
        provider.trigger('change');
        for (let i in f.validator) {
            validator.find('option[value="'+f.validator[i]+'"]').prop('selected', true);
        }
        validator.trigger('change');

        form.attr('data-step', step);
        form.attr('data-field', field);
        $('#field-modal').modal('show');
    }
</script>
<script type="text/javascript">
    <?php if (count($model->serviceProcesses) > 0): ?>
    let serviceStages = <?=\yii\helpers\Json::encode($model->serviceProcesses)?>;
    <?php else: ?>
    let serviceStages = [];
    <?php endif; ?>

    (function ($) {
        renderProcesses($);
    })(jQuery);

    function addProcess() {
        let status = $('#process-status');
        let stage = $('#process-stage');

        if (status.val() === '' || stage.val() === '') {
            alert('Заполните все поля процесса');
            return;
        }

        serviceStages.push({
            status: status.val(),
            stage: stage.val(),
        });

        console.log(serviceStages);

        status.val('');
        // stage.val('');
        renderProcesses(jQuery);
    }

    function removeProcess(row) {
        delete serviceStages[row];
        renderProcesses(jQuery);
    }


    function renderProcesses($) {
        const stages = {
            'new': 'Заявление поступило',
            'incomplete': 'Неполные данные',
            'accepted': 'Принято к рассмотрению',
            'reviewed': 'Рассмотрено',
        };

        let html = '';
        let tpl = $('#template-process').html();
        console.log(serviceStages);
        for (let i in serviceStages) {
            if (serviceStages[i]) {
                console.log(serviceStages[i]);
                let row = tpl.replace(/{{status}}/g, serviceStages[i].status);
                row = row.replace(/{{stage}}/g, stages[serviceStages[i].stage]);
                row = row.replace(/{{row}}/g, i);
                html += row;
            }
        }

        $('#statuses tbody').html(html);
    }
</script>

